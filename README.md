# ~/ABRÜPT/ÉTIENNE MICHELET/TOUCAN FANTÔME/*

La [page de ce livre](https://abrupt.cc/etienne-michelet/toucan-fantome/) sur le réseau.

## Sur le livre

Toco le toucan, oiseau prophylactique, n’a d’autre espoir que de sortir l’Enfance des catacombes. Toco veut sauver l’enfant Rosalia Lombardo, morte d’une pneumonie en 1920 à l’âge de deux ans, et dont le corps momifié est encore aujourd’hui exposé dans les catacombes des Capucins à Palerme. Mais avant que le toucan se penche sur Rosalia, le temps est au règlement de compte, puisque l'époque est aux momies. L'oiseau ne sera pas le jouet de l'humain, de sa taxidermie fétiche, il volera et volera sa langue, y déposera le délire au plus près du verbe. Le toucan saura se faire psychotrope sublingual, pour révéler ce qu'il reste d'avant les mots, ce qu'il y reste encore de joie.

## Sur l'auteur

Né en 1984. Vit en Corée du Sud.
Écrit un journal, avec animaux, lune, fleurs et des fantômes aussi.
Enregistre sa voix, parfois, pour y voir mieux.

Son site : [DONGMURI](https://www.dongmuri.net/).

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).

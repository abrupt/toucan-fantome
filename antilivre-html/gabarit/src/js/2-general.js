// Scripts

// Musique
var audio = document.getElementById('fond-sonore');
// var musiqueJoue = "pause";
function fondSonore() {
  if (audio.paused) {
    audio.play();
  } else {
    audio.pause();
  }
}

// Menu
const menu = document.querySelector('.menu');
const titre = document.querySelector('.pagetitre');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  titre.classList.toggle('show--menu');
  document.querySelector('.menu .info').classList.toggle('info--enable');
});

const musique = document.querySelector('.musique');
musique.addEventListener('click', (e) => {
  e.preventDefault();
  document.querySelector('.musique .info').classList.toggle('info--enable');
  fondSonore();
});

//
// Tools & General Functions
//
function randomNb(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

//
// Interact.js
//
const angleScale = {
  angle: 0,
  scale: 1,
};
let zIndexDrag = 0;

const targetInteract = '.texte__button';
interact(targetInteract)
  .styleCursor(false)
  .draggable({
    listeners: {
      start: function (event) {
        event.target.style.zIndex = parseInt(new Date().getTime() / 10 ** 10) + zIndexDrag;
        zIndexDrag += 1;
        event.target.style.pointerEvents = 'none';
      },
      move: dragMoveListener,
      end: function (event) {
        event.target.style.pointerEvents = '';
      },
    },
    inertia: true,
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: '.container',
        endOnly: true,
      }),
    ],
    autoScroll: false,
  })
  .gesturable({
    // enable autoScroll
    autoScroll: false,
    listeners: {
      start: function (event) {
        angleScale.angle -= event.angle;
      },
      move: function (event) {
        // document.body.appendChild(new Text(event.scale))
        let currentAngle = event.angle + angleScale.angle;
        let currentScale = event.scale * angleScale.scale;

        let scaleElement = event.target.querySelector('img');
        scaleElement.style.webkitTransform = scaleElement.style.transform =
          'rotate(' + currentAngle + 'deg)' + 'scale(' + currentScale + ')';

        // uses the dragMoveListener from the draggable demo above
        dragMoveListener(event);
      },
      end(event) {
        angleScale.angle = angleScale.angle + event.angle;
        angleScale.scale = angleScale.scale * event.scale;
      },
    },
    modifiers: [
      interact.modifiers.aspectRatio({
        ratio: 'preserve',
      }),
      interact.modifiers.restrictSize({
        min: { width: 40, height: 40 }
      })
    ],
    inertia: true
  });

function dragMoveListener(event) {
  let target = event.target;
  // keep the dragged position in the data-x/data-y attributes
  let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
  let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform = target.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

//
// Animate images
//
const images = document.querySelectorAll('.texte__button');

for (const el of images) {
  el.style.zIndex = randomNb(1, images.length);
  Pace.on('done', function () {
    animateDiv(el);
  });
}

function animateDiv(el) {
  let x = (Math.random() < 0.5 ? -1 : 1) * randomNb(20, 200);
  let y = (Math.random() < 0.5 ? -1 : 1) * randomNb(20, 200);
  let updates = 0;
  anime({
    targets: el,
    translateX: x,
    translateY: y,
    duration: randomNb(2000, 4000),
    easing: 'easeOutCubic',
    complete: function (anim) {
      el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
      el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
    },
  });
}

// Show texts
const textBtns = document.querySelectorAll('.texte__button');
const texts = document.querySelectorAll('.texte');

for (const btn of textBtns) {
  btn.addEventListener('click', (e) => {
    const relatedText = document.querySelector(`.texte[data-image="${btn.dataset.image}"`);
    relatedText.classList.toggle('hide');
  });
}

// For mobile (with Interact)
if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
  interact(targetInteract).on('tap', function (event) {
    const relatedTextMob = document.querySelector(`.texte[data-image="${event.currentTarget.dataset.image}"]`);
    relatedTextMob.classList.toggle('hide');
  });

  function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
      var v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
      return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
  }
  ver = iOSversion();

  if (ver[0] <= 13) {
    const banner = document.createElement('div');
    banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 90000; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#1a3729; color: #ddffee;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cet antilivre fonctionne totalement sur les versions supérieures à iOS 13.<br>(La piraterie littéraire n’est jamais finie.)</p></div>`;
    banner.onclick = () => banner.remove();
    document.querySelector('body').appendChild(banner);
  }
}

const close = document.querySelectorAll('.close');
close.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    e.preventDefault();
    btn.parentNode.classList.toggle('hide');
  });
});

// Custom Cursor
const cursor = document.querySelector('.cursor--main');
const cursorBg = document.querySelector('.cursor--bg');
const links = document.querySelectorAll('button, a');

let cursorX = -100;
let cursorY = -100;
let cursorBgX = -100;
let cursorBgY = -100;
let pageLoad = true;

const cursorMove = () => {
  document.addEventListener('mousemove', (e) => {
    cursorX = e.clientX;
    cursorY = e.clientY;
    if (pageLoad) {
      cursorBgX = e.clientX;
      cursorBgY = e.clientY;
      pageLoad = !pageLoad;
    }
  });

  // Lerp (linear interpolation)
  const lerp = (start, finish, speed) => {
    return (1 - speed) * start + speed * finish;
  };

  const rendering = () => {
    cursorBgX = lerp(cursorBgX, cursorX, 0.1);
    cursorBgY = lerp(cursorBgY, cursorY, 0.1);
    cursor.style.left = `${cursorX}px`;
    cursor.style.top = `${cursorY}px`;
    cursorBg.style.left = `${cursorBgX}px`;
    cursorBg.style.top = `${cursorBgY}px`;
    requestAnimationFrame(rendering);
  };
  requestAnimationFrame(rendering);
};

cursorMove();

// Interaction with the cursor
links.forEach((e) => {
  e.addEventListener('mouseenter', () => {
    cursor.classList.add('cursor--interact');
    cursorBg.classList.add('cursor--interact-bg');
  });
  e.addEventListener('mouseleave', () => {
    cursor.classList.remove('cursor--interact');
    cursorBg.classList.remove('cursor--interact-bg');
  });
});

document.addEventListener('click', () => {
  cursor.classList.add('cursor--click');

  setTimeout(() => {
    cursor.classList.remove('cursor--click');
  }, 250);
});

//
// Corrections & Bug fixes
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var container = document.querySelector('.container');
var containerTitle = document.querySelector('.pagetitre');
container.addEventListener('touchstart', function (event) {
  this.allowUp = this.scrollTop > 0;
  this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
  this.slideBeginY = event.pageY;
});

container.addEventListener('touchmove', function (event) {
  var up = event.pageY > this.slideBeginY;
  var down = event.pageY < this.slideBeginY;
  this.slideBeginY = event.pageY;
  if ((up && this.allowUp) || (down && this.allowDown)) {
    event.stopPropagation();
  } else {
    event.preventDefault();
  }
});

containerTitle.addEventListener('touchstart', function (event) {
  this.allowUp = this.scrollTop > 0;
  this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
  this.slideBeginY = event.pageY;
});

containerTitle.addEventListener('touchmove', function (event) {
  var up = event.pageY > this.slideBeginY;
  var down = event.pageY < this.slideBeginY;
  this.slideBeginY = event.pageY;
  if ((up && this.allowUp) || (down && this.allowDown)) {
    event.stopPropagation();
  } else {
    event.preventDefault();
  }
});
